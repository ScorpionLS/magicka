﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Spell: MonoBehaviour
{
    protected Queue<MagicAction> actionQueue
    {
        get;
        set;
    } = new Queue<MagicAction>();
    
    public void AddAction(MagicAction magicAction)
    {
        actionQueue.Enqueue(magicAction);
    }

    public void StartCast() => StartCastHandler();

    protected abstract void StartCastHandler();

    protected virtual void EndCastHandler()
    {
        if (actionQueue.Count > 0)
        {
            actionQueue.Dequeue().ApplyToSpell(this);
        }
    }

    public abstract void ApplyMultplyAction();

    public abstract void DestroySpell();
}
