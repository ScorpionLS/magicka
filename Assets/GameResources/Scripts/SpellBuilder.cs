﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellBuilder
{
    private readonly IMagicViewFabric<Spell> spellFabric;
    
    private Magic currentMagic = new EmptyMagic();

    private  List<Spell> spells = new List<Spell>();

    private bool isActionState = false;

    public SpellBuilder(IMagicViewFabric<Spell> magicViewFabric)
    {
        spellFabric = magicViewFabric;
    }

    public void AddType(MagicType magicType)
    {
        if (isActionState)
        {
            currentMagic = new EmptyMagic();
            isActionState = false;
        }
        currentMagic += magicType;
    }

    public void AddAction(MagicAction magicAction)
    {
        if (isActionState)
        {
            spells[spells.Count - 1].AddAction(magicAction);
            Debug.LogError("TERAHER");
        }
        else
        {
            var spell = currentMagic.GetMagicView(spellFabric);
            spell.AddAction(magicAction);
            spells.Add(spell);
            isActionState = true;
        }
    }

    public Spell[] GetSpells()
    {
        var ret = spells.ToArray();
        spells.Clear();
        return ret;
    }
}
