﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMagicViewFabric<T>
{
    /// <summary>
    /// Получить въюшку магии огня
    /// </summary>
    /// <param name="???"></param>
    /// <returns></returns>
    T GetFireView(FireMagic fireMagic);

    T GetWaterView(WaterMagic waterMagic);
}