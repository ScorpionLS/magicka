﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using Test;
using UnityEngine;

public abstract class MagicAction
{
    public abstract void ApplyToSpell(Spell spell);
}

public class MultiplyMagic : MagicAction
{
    public override void ApplyToSpell(Spell spell)
    {
        spell.ApplyMultplyAction();
    }
}
