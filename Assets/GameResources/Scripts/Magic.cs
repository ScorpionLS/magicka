﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Magic
{
    public static Magic operator +(Magic magic, MagicType magicType)
    {
        return magicType.ApplyToMagic(magic);
    }

    public abstract Magic AddFire();

    public abstract T GetMagicView<T>(IMagicViewFabric<T> magicViewFabric);
}

public class EmptyMagic : Magic
{
    public override Magic AddFire()
    {
        return  new FireMagic();
    }

    public override T GetMagicView<T>(IMagicViewFabric<T> magicViewFabric)
    {
        return default;
    }
}

public class FireMagic : Magic
{
    public override Magic AddFire()
    {
        return this;
    }

    public override T GetMagicView<T>(IMagicViewFabric<T> magicViewFabric)
    {
        return magicViewFabric.GetFireView(this);
    }
}

public class WaterMagic : Magic
{
    public override Magic AddFire()
    {
        return null;
    }

    public override T GetMagicView<T>(IMagicViewFabric<T> magicViewFabric)
    {
        return magicViewFabric.GetWaterView(this);
    }
}
