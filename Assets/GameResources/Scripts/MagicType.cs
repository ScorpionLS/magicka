﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMagicTypeViewFabric<T>
{
    T GetFireTypeView(FireType fireType);
}

public abstract class MagicType
{
    public abstract Magic ApplyToMagic(Magic magic);

    public abstract T GetMagicTypeView<T>(IMagicTypeViewFabric<T> magicTypeViewFabric);
}

public class FireType : MagicType
{
    public override Magic ApplyToMagic(Magic magic)
    {
        return magic.AddFire();
    }

    public override T GetMagicTypeView<T>(IMagicTypeViewFabric<T> magicTypeViewFabric)
    {
        return magicTypeViewFabric.GetFireTypeView(this);
    }
}
