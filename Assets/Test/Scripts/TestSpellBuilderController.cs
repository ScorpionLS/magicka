﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Test
{
    public class TestSpellBuilderController : MonoBehaviour
    {
        private readonly SpellBuilder spellBuilder = new SpellBuilder(new UIMagicFabricSpells());

        private  readonly  IMagicTypeViewFabric<Image> _magicTypeViewFabric = new UIMagicTypeViewFabric();

        [SerializeField] private Transform horizontalLayout;

        [SerializeField] private Transform spellHolder;
        
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                var FireTypo = new FireType();
                FireTypo.GetMagicTypeView(_magicTypeViewFabric).transform.SetParent(horizontalLayout);
                spellBuilder.AddType(FireTypo);
            }
            if (Input.GetKeyDown(KeyCode.Y))
            {
                spellBuilder.AddAction(new MultiplyMagic());
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                foreach(var spell in spellBuilder.GetSpells())
                {
                    spell.transform.SetParent(spellHolder);
                    spell.StartCast();
                }
            }
        }
    }
    
    

    public class UIMagicTypeViewFabric : IMagicTypeViewFabric<Image>
    {
        Image IMagicTypeViewFabric<Image>.GetFireTypeView(FireType fireType)
        {
            var im = (new GameObject("Fire")).AddComponent<Image>();
            im.sprite = Resources.Load<Sprite>("TestFire");
            im.preserveAspect = true;
            return im;
        }
    }
    
    public class UIMagicFabricSpells : IMagicViewFabric<Spell>
    {
        public class TestSpell : Spell 
        {
            private void Awake()
            {
                gameObject.SetActive(false);
            }

            protected override void StartCastHandler()
            {
                gameObject.SetActive(true);
                StartCoroutine(Anime());
            }

            private IEnumerator Anime()
            {
                var im = GetComponent<Image>();
                im.color = new Color(1, 1, 1,0);
                float t = 0;
                while (t <= 1)
                {
                    im.color = new Color(1,1, 1, t);
                    yield return  null;
                    t += Time.deltaTime;
                }
                EndCastHandler();
            }

            public override void ApplyMultplyAction()
            {
                for(int i = 0; i < 3; i++)
                {
                    var newSP = Instantiate(this, transform.parent);
                    newSP.actionQueue = actionQueue;
                    newSP.StartCast();
                }
            }

            public override void DestroySpell()
            {
            }
        }
        
        Spell IMagicViewFabric<Spell>.GetFireView(FireMagic fireMagic)
        {
            var spell = (new GameObject("FireSpell")).AddComponent<Image>();
            spell.sprite = Resources.Load<Sprite>("TestFire");
            spell.preserveAspect = true;
            return spell.gameObject.AddComponent<TestSpell>();
        }

        Spell IMagicViewFabric<Spell>.GetWaterView(WaterMagic waterMagic)
        {
            throw new System.NotImplementedException();
        }
    }
}
